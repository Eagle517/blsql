# BLsql
Implementation of SQLite3 for Blockland.

## Security Notice
This DLL does not prevent the ability for `FileObject::sqliteOpen` to open files outside of your Blockland directory, similar to BlocklandLua.

## Usage
```
bool   FileObject::sqliteOpen(file database)
bool   FileObject::sqliteClose()
bool   FileObject::sqliteExec(string stmt)
bool   FileObject::sqliteIsOpen()
int    FileObject::sqliteGetErrorCode()
string FileObject::sqliteGetErrorMsg()
string FileObject::sqliteGetResult()
int    FileObject::sqliteGetResultCount()
```

Any bool method will return true upon success or false upon failure.
The `sqliteGetErrorCode` and `sqliteGetErrorMsg` methods can be useful
for debugging.

`sqliteGetResult` will return each row one-by-one as a tab-delimited list until none are left, upon which it will return an empty string. The first row will always be the name of the columns. `sqliteGetResultCount` will return how many rows are left. Note that executing another statement will clear any results left.

It is very important that you call `sqliteClose` prior to deleting the `FileObject`. The memory used by the DLL does not free itself nor does the file close itself unless you call this method.

The DLL(s) available are all compiled with the flag `DEFINE_SQLITE_GLOBALS` which defines all of the standard SQLite error codes as global variables in TorqueScript. The global variables are:
```
$SQLITE::OK
$SQLITE::ERROR
$SQLITE::INTERNAL
$SQLITE::PERM
$SQLITE::ABORT
$SQLITE::BUSY
$SQLITE::LOCKED
$SQLITE::NOMEM
$SQLITE::READONLY
$SQLITE::INTERRUPT
$SQLITE::IOERR
$SQLITE::CORRUPT
$SQLITE::NOTFOUND
$SQLITE::FULL
$SQLITE::CANTOPEN
$SQLITE::PROTOCOL
$SQLITE::EMPTY
$SQLITE::SCHEMA
$SQLITE::TOOBIG
$SQLITE::CONSTRAINT
$SQLITE::MISMATCH
$SQLITE::MISUSE
$SQLITE::NOLFS
$SQLITE::AUTH
$SQLITE::FORMAT
$SQLITE::RANGE
$SQLITE::NOTADB
$SQLITE::NOTICE
$SQLITE::WARNING
$SQLITE::ROW
$SQLITE::DONE
```
