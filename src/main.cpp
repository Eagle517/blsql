#include <string>
#include <map>
#include <queue>
#include <windows.h>

#include "sqlite/sqlite3.h"
#include "TSFuncs.hpp"

struct sqliteFile
{
	sqlite3 *db;
	std::queue<std::string> queryResults;

	sqliteFile() : db(NULL) {}
};

std::map<unsigned int, sqliteFile *> gFileMap;

BlFunctionDef(char *, __fastcall, Con__getReturnBuffer, unsigned int);

unsigned int getSimObjectID(ADDR obj)
{
	return *(unsigned int *)(obj + 32);
}

sqliteFile *getSqliteFile(ADDR obj)
{
	unsigned int id = getSimObjectID(obj);

	if(gFileMap.count(id) == 0)
		return NULL;
	else
		return gFileMap[id];
}

void insertSqliteFile(ADDR obj, sqliteFile *file)
{
	unsigned int id = getSimObjectID(obj);
	gFileMap.insert(std::make_pair(id, file));
}

void eraseSqliteFile(ADDR obj)
{
	sqliteFile *file = getSqliteFile(obj);

	if(file != NULL)
	{
		if(file->db != NULL)
			sqlite3_close(file->db);

		gFileMap.erase(getSimObjectID(obj));
		delete file;
	}
}

static int sqlite3Callback(void *param, int argc, char **argv, char **azColName)
{
	sqliteFile *file = (sqliteFile *)param;

	if(file->queryResults.empty())
	{
		std::string columns = azColName[0];
		for(int i = 1; i < argc; ++i)
		{
			columns += std::string("\t");
			columns += std::string(azColName[i]);
		}

		file->queryResults.push(columns);
	}

	std::string result = std::string(argv[0]);
	for(int i = 1; i < argc; ++i)
	{
		result += std::string("\t");
		result += std::string(argv[i]);
	}

	file->queryResults.push(result);

	return SQLITE_OK;
}

bool TS_FileObject__sqliteOpen(ADDR obj, int argc, const char *argv[])
{
	if(getSqliteFile(obj) != NULL)
		return false;

	sqliteFile *file = new sqliteFile();
	insertSqliteFile(obj, file);

	return sqlite3_open(argv[2], &file->db) == SQLITE_OK;
}

bool TS_FileObject__sqliteClose(ADDR obj, int argc, const char *argv[])
{
	sqliteFile *file = getSqliteFile(obj);

	if(file == NULL)
		return true;

	int rc = sqlite3_close(file->db);

	if(rc == SQLITE_OK)
		eraseSqliteFile(obj);

	return rc == SQLITE_OK;
}

bool TS_FileObject__sqliteIsOpen(ADDR obj, int argc, const char *argv[])
{
	sqliteFile *file = getSqliteFile(obj);
	return file != NULL && file->db != NULL;
}

bool TS_FileObject__sqliteExec(ADDR obj, int argc, const char *argv[])
{
	sqliteFile *file = getSqliteFile(obj);

	if(file == NULL)
		return false;

	return sqlite3_exec(file->db, argv[2], sqlite3Callback, file, NULL) == SQLITE_OK;
}

int TS_FileObject__sqliteGetErrorCode(ADDR obj, int argc, const char *argv[])
{
	sqliteFile *file = getSqliteFile(obj);
	return file == NULL ? SQLITE_OK : sqlite3_errcode(file->db);
}

const char *TS_FileObject__sqliteGetErrorMsg(ADDR obj, int argc, const char *argv[])
{
	sqliteFile *file = getSqliteFile(obj);
	return file == NULL ? sqlite3_errstr(SQLITE_OK) : sqlite3_errmsg(file->db);
}

const char *TS_FileObject__sqliteGetResult(ADDR obj, int argc, const char *argv[])
{
	sqliteFile *file = getSqliteFile(obj);

	if(file == NULL || file->queryResults.empty())
		return "";

	std::string result = file->queryResults.front();
	char *ret = Con__getReturnBuffer(result.length() + 1);
	sprintf(ret, "%s", result.c_str());

	file->queryResults.pop();

	return (const char *)ret;
}

int TS_FileObject__sqliteGetResultCount(ADDR obj, int argc, const char *argv[])
{
	sqliteFile *file = getSqliteFile(obj);
	return file == NULL ? 0 : file->queryResults.size();
}

bool init()
{
	BlInit;

	if(!tsf_InitInternal())
		return false;

	BlScanFunctionHex(Con__getReturnBuffer, "81 F9 ? ? ? ? 76 2B");

	tsf_AddConsoleFunc(NULL, "FileObject", "sqliteOpen",           TS_FileObject__sqliteOpen,           "(file database) - Opens the database for SQL queries",           3, 3);
	tsf_AddConsoleFunc(NULL, "FileObject", "sqliteClose",          TS_FileObject__sqliteClose,          "Closes the currently opened database",                           2, 2);
	tsf_AddConsoleFunc(NULL, "FileObject", "sqliteExec",           TS_FileObject__sqliteExec,           "(string query) - Queries the opened database",                   3, 3);
	tsf_AddConsoleFunc(NULL, "FileObject", "sqliteIsOpen",         TS_FileObject__sqliteIsOpen,         "Returns whether or not a database is opened by this FileObject", 2, 2);
	tsf_AddConsoleFunc(NULL, "FileObject", "sqliteGetErrorCode",   TS_FileObject__sqliteGetErrorCode,   "Returns the last error code",                                    2, 2);
	tsf_AddConsoleFunc(NULL, "FileObject", "sqliteGetErrorMsg",    TS_FileObject__sqliteGetErrorMsg,    "Returns a readable message that describes the last error",       2, 2);
	tsf_AddConsoleFunc(NULL, "FileObject", "sqliteGetResult",      TS_FileObject__sqliteGetResult,      "Returns the next query result as a tab delimited list",          2, 2);
	tsf_AddConsoleFunc(NULL, "FileObject", "sqliteGetResultCount", TS_FileObject__sqliteGetResultCount, "Returns the number of query results not yet returned",           2, 2);

#ifdef DEFINE_SQLITE_GLOBALS
	tsf_Evalf("$SQLITE::OK         = %d;"
	          "$SQLITE::ERROR      = %d;"
	          "$SQLITE::INTERNAL   = %d;"
	          "$SQLITE::PERM       = %d;"
	          "$SQLITE::ABORT      = %d;"
	          "$SQLITE::BUSY       = %d;"
	          "$SQLITE::LOCKED     = %d;"
	          "$SQLITE::NOMEM      = %d;"
	          "$SQLITE::READONLY   = %d;"
	          "$SQLITE::INTERRUPT  = %d;"
	          "$SQLITE::IOERR      = %d;"
	          "$SQLITE::CORRUPT    = %d;"
	          "$SQLITE::NOTFOUND   = %d;"
	          "$SQLITE::FULL       = %d;"
	          "$SQLITE::CANTOPEN   = %d;"
	          "$SQLITE::PROTOCOL   = %d;"
	          "$SQLITE::EMPTY      = %d;"
	          "$SQLITE::SCHEMA     = %d;"
	          "$SQLITE::TOOBIG     = %d;"
	          "$SQLITE::CONSTRAINT = %d;"
	          "$SQLITE::MISMATCH   = %d;"
	          "$SQLITE::MISUSE     = %d;"
	          "$SQLITE::NOLFS      = %d;"
	          "$SQLITE::AUTH       = %d;"
	          "$SQLITE::FORMAT     = %d;"
	          "$SQLITE::RANGE      = %d;"
	          "$SQLITE::NOTADB     = %d;"
	          "$SQLITE::NOTICE     = %d;"
	          "$SQLITE::WARNING    = %d;"
	          "$SQLITE::ROW        = %d;"
	          "$SQLITE::DONE       = %d;",

	           SQLITE_OK,
	           SQLITE_ERROR,
	           SQLITE_INTERNAL,
	           SQLITE_PERM,
	           SQLITE_ABORT,
	           SQLITE_BUSY,
	           SQLITE_LOCKED,
	           SQLITE_NOMEM,
	           SQLITE_READONLY,
	           SQLITE_INTERRUPT,
	           SQLITE_IOERR,
	           SQLITE_CORRUPT,
	           SQLITE_NOTFOUND,
	           SQLITE_FULL,
	           SQLITE_CANTOPEN,
	           SQLITE_PROTOCOL,
	           SQLITE_EMPTY,
	           SQLITE_SCHEMA,
	           SQLITE_TOOBIG,
	           SQLITE_CONSTRAINT,
	           SQLITE_MISMATCH,
	           SQLITE_MISUSE,
	           SQLITE_NOLFS,
	           SQLITE_AUTH,
	           SQLITE_FORMAT,
	           SQLITE_RANGE,
	           SQLITE_NOTADB,
	           SQLITE_NOTICE,
	           SQLITE_WARNING,
	           SQLITE_ROW,
	           SQLITE_DONE
	);
#endif

	BlPrintf("%s: init'd", PROJECT_NAME);
	return true;
}

bool deinit()
{
	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
