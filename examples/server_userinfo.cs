function UserInfo::reportError()
{
	%err = $UserInfoFO.sqliteGetErrorCode();
	%msg = $UserInfoFO.sqliteGetErrorMsg();
	echo("\x03UserInfo sqlite3 error (", %err, "): ", %msg);
}

function UserInfo::init()
{
	if(isObject($UserInfoFO))
	{
		$UserInfoFO.sqliteClose();
		$UserInfoFO.delete();
	}

	$UserInfoFO = new FileObject();
	if($UserInfoFO.sqliteOpen("config/server/userinfo.db") == false)
	{
		UserInfo::reportError();
		return;
	}

	UserInfo::exec("CREATE TABLE IF NOT EXISTS UserInfo (ID INT PRIMARY KEY, BLID INT UNIQUE NOT NULL, Name TEXT NOT NULL);");
}

function UserInfo::deinit()
{
	if(isObject($UserInfoFO))
	{
		$UserInfoFO.sqliteClose();
		$UserInfoFO.delete();
		deleteVariables("$UserInfoFO");
	}
}

function UserInfo::exec(%stmt)
{
	if($UserInfoFO.sqliteExec(%stmt) == false)
	{
		UserInfo::reportError();
		return false;
	}
	else
		return true;
}

function UserInfo::insert(%blid, %name)
{
	%stmt = "INSERT INTO UserInfo (BLID, Name) " @
	            "VALUES(" @ %blid @ ", '" @ %name @ "')" @
	            "ON CONFLICT(BLID)" @
	            "DO UPDATE SET Name=excluded.Name";

	return UserInfo::exec(%stmt);
}

function UserInfo::getResult()
{
	return $UserInfoFO.sqliteGetResult();
}

function UserInfo::getResultCount()
{
	return $UserInfoFO.sqliteGetResultCount();
}

if(!isObject($UserInfoFO))
	UserInfo::init();

//Searches for an exact BLID
function serverCmdSearchBLID(%client, %blid)
{
	if(!isObject($UserInfoFO))
		return;

	if(%blid $= "" || mFloor(%blid) !$= %blid || %blid < 0)
	{
		messageClient(%client, '', '\c6Please enter a valid BLID.');
		return;
	}

	%blid = %blid | 0; //Just to be safe

	%stmt = "SELECT BLID, Name FROM UserInfo WHERE BLID=" @ %blid @ ";";

	if(UserInfo::exec(%stmt))
	{
		if(UserInfo::getResultCount() > 0)
		{
			UserInfo::getResult(); //The first result is always the column names

			%info = UserInfo::getResult();
			%blid = getField(%info, 0);
			%name = getField(%info, 1);

			messageClient(%client, '', '\c6BLID \c3%1\c6 belongs to \c3%2\c6.', %blid, %name);
		}
		else
			messageClient(%client, '', '\c6That BLID was not found.');
	}
}

//Searches for a partial name and gives the first 50 results
function serverCmdSearchName(%client, %n0, %n1, %n2, %n3, %n4, %n5)
{
	if(!isObject($UserInfoFO))
		return;

	%name = %n0;
	for(%i = 1; %i <= 5; %i++)
		%name = %name SPC %n[%i];

	%name = trim(%name);

	if(%name $= "")
	{
		messageClient(%client, '', '\c6Please enter a valid name.');
		return;
	}

	%stmt = "SELECT BLID, Name FROM UserInfo WHERE Name LIKE '%" @ %name @ "%' LIMIT 50;";

	if(UserInfo::exec(%stmt))
	{
		if(UserInfo::getResultCount() > 0)
		{
			UserInfo::getResult(); //The first result is always the column names

			while(UserInfo::getResultCount())
			{
				%info = UserInfo::getResult();
				%blid = getField(%info, 0);
				%name = getField(%info, 1);

				messageClient(%client, '', '\c6BLID \c3%1\c6 belongs to \c3%2\c6.', %blid, %name);
			}
		}
		else
			messageClient(%client, '', '\c6That name was not found.');
	}
}

//Lists entries in chunks of 50
function serverCmdListUserInfo(%client, %page)
{
	if(!isObject($UserInfoFO))
		return;

	%page = %page | 0;
	if(%page < 0)
		%page = 0;

	%offset = 50 * %page;
	%stmt = "SELECT BLID, Name FROM UserInfo ORDER BY BLID ASC LIMIT 50 OFFSET " @ %offset @ ";";

	if(UserInfo::exec(%stmt))
	{
		if(UserInfo::getResultCount() > 0)
		{
			UserInfo::getResult(); //The first result is always the column names

			while(UserInfo::getResultCount())
			{
				%info = UserInfo::getResult();
				%blid = getField(%info, 0);
				%name = getField(%info, 1);

				messageClient(%client, '', '\c6BLID \c3%1\c6 belongs to \c3%2\c6.', %blid, %name);
			}
		}
		else
			messageClient(%client, '', '\c6There is no data to display.');
	}
}

package Server_UserInfo
{
	//Automatically insert the user into the database upon joining
	function GameConnection::autoAdminCheck(%this)
	{
		%ret = parent::autoAdminCheck(%this);

		if(isObject($UserInfoFO))
		{
			%name = %this.getPlayerName();
			%blid = %this.getBLID();

			UserInfo::insert(%blid, %name);
		}

		return %ret;
	}

	//Automatically clean up when the server shuts down
	function onServerDestroyed()
	{
		UserInfo::deinit();
		parent::onServerDestroyed();
	}
};
activatePackage("Server_UserInfo");
